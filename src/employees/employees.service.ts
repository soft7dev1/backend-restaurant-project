import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { CreateEmployeeDto } from './dto/create-employee.dto';
import { UpdateEmployeeDto } from './dto/update-employee.dto';
import { Employee } from './entities/employee.entity';

@Injectable()
export class EmployeesService {
  constructor(
    @InjectRepository(Employee)
    private employeesRepository: Repository<Employee>,
  ) {}

  create(createEmployeeDto: CreateEmployeeDto) {
    return this.employeesRepository.save(createEmployeeDto);
  }

  findAll() {
    return this.employeesRepository.find();
  }

  findOne(id: number) {
    return this.employeesRepository.findOne({ where: { id: id } });
  }

  findOneByUsername(username: string) {
    return this.employeesRepository.findOne({ where: { username: username } });
  }
  async update(id: number, updateEmployeeDto: UpdateEmployeeDto) {
    try {
      const updatedEmployee = await this.employeesRepository.save({
        id,
        ...updateEmployeeDto,
      });
      return updatedEmployee;
    } catch (e) {
      throw new NotFoundException();
    }
  }

  async remove(id: number) {
    const employee = await this.employeesRepository.findOne({
      where: { id: id },
    });
    try {
      const deleteEmployee = await this.employeesRepository.remove(employee);
      return deleteEmployee;
    } catch (e) {
      throw new NotFoundException();
    }
  }
}
