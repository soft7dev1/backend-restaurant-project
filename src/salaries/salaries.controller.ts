import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
  Query,
} from '@nestjs/common';
import { SalariesService } from './salaries.service';
import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { query } from 'express';

@Controller('salaries')
export class SalariesController {
  constructor(private readonly salariesService: SalariesService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createSalaryDto: CreateSalaryDto) {
    return this.salariesService.create(createSalaryDto);
  }
  @Get('year')
  getYear() {
    return this.salariesService.getYear();
  }
  @Get('salarydate')
  getSalaryDate(@Query() query: { startD: string; endD: string }) {
    console.log(query);
    return this.salariesService.getSalaryDate(query.startD, query.endD);
    // {{server}}/salaries/salarydate?startD=2023-03-20&endD=2023-03-25
  }
  @Get('salarycio')
  getSalaryCIO(@Query() query: { startD: string; endD: string }) {
    console.log(query);
    return this.salariesService.getCheckIO(query.startD, query.endD);
    // {{server}}/salaries/salarydate?startD=2023-03-20&endD=2023-03-25
  }
  @Get()
  findAll() {
    return this.salariesService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.salariesService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateSalaryDto: UpdateSalaryDto) {
    return this.salariesService.update(+id, updateSalaryDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.salariesService.remove(+id);
  }
}
