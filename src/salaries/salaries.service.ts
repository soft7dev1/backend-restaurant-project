import { Injectable, NotFoundException } from '@nestjs/common';

import { CreateSalaryDto } from './dto/create-salary.dto';
import { UpdateSalaryDto } from './dto/update-salary.dto';
import { Salary } from './entities/salary.entity';
import { InjectDataSource, InjectRepository } from '@nestjs/typeorm';
import { DataSource, Repository } from 'typeorm';
import { SalaryDetail } from './entities/salary-detail';
import { Employee } from 'src/employees/entities/employee.entity';
import { Checkinout } from 'src/checkinout/entities/checkinout.entity';

@Injectable()
export class SalariesService {
  constructor(
    @InjectRepository(Salary)
    private salariesRepository: Repository<Salary>,
    @InjectRepository(SalaryDetail)
    private salaryItemRepository: Repository<SalaryDetail>,
    @InjectRepository(Employee)
    private employeeRepository: Repository<Employee>,
    @InjectRepository(Checkinout)
    private checkinoutRepository: Repository<Checkinout>,
    @InjectDataSource() private dataSource: DataSource,
  ) {}

  getYear() {
    return this.dataSource.query(
      `SELECT DISTINCT strftime('%Y', datetimeIn) as Year FROM checkinout`,
    );
  }
  getSalaryDate(startD: string, endD: string) {
    return this.dataSource.query(
      'SELECT employee.id AS emp_id, employee.name AS emp_name,  SUM(checkinout.total_hour) AS emp_whours, employee.sal_rate AS emp_rate, ROUND(SUM(employee.sal_rate*total_hour)) AS emp_total' +
        ' FROM checkinout INNER JOIN employee ON checkinout.employeeId = employee.id ' +
        ' WHERE datetimeIn BETWEEN ? AND ? AND checkinout.salaryDetailId is null  AND checkinout.total_hour>0 GROUP BY checkinout.employeeId',
      [`${startD}`, `${endD}`],
    );
  }

  getCheckIO(startD: string, endD: string) {
    return this.dataSource.query(
      'SELECT checkinout.datetimeIn, employee.name, checkinout.total_hour, employee.sal_rate,  checkinout.salaryDetailId, checkinout.id' +
        ' FROM checkinout INNER JOIN employee ON checkinout.employeeId = employee.id ' +
        ' WHERE datetimeIn BETWEEN ? AND ? AND checkinout.salaryDetailId is null  AND checkinout.total_hour>0',
      [`${startD}`, `${endD}`],
    );
  }

  async create(createSalaryDto: CreateSalaryDto) {
    console.log(createSalaryDto.salaryDetailLists);
    const salary: Salary = new Salary();
    salary.date_start = createSalaryDto.date_start;
    salary.date_end = createSalaryDto.date_end;
    salary.date_salary = createSalaryDto.date_salary;
    salary.total = 0;
    await this.salariesRepository.save(salary);
    for (const sr of createSalaryDto.salaryDetailLists) {
      const salaryItem = new SalaryDetail();
      salaryItem.employeeId = await this.employeeRepository.findOneBy({
        id: sr.emp_id,
      });
      salaryItem.emp_name = salaryItem.employeeId.name;
      salaryItem.emp_whours = sr.emp_whours;
      salaryItem.emp_rate = salaryItem.employeeId.sal_rate;
      salaryItem.emp_total = salaryItem.emp_rate * salaryItem.emp_whours;
      salaryItem.salaryId = salary;
      const salItemfn = await this.salaryItemRepository.save(salaryItem);
      salary.total += salaryItem.emp_total;
      for (const c of createSalaryDto.cionum) {
        const cio = await this.checkinoutRepository.findOneBy({
          id: c,
          employee: { id: salItemfn.employeeId.id },
        });
        console.log(salItemfn);
        if (cio) {
          console.log(cio.salaryDetail);
          cio.salaryDetail = salItemfn;
          console.log('======');
          console.log(cio);
          await this.checkinoutRepository.save(cio);
        }
      }
    }
    await this.salariesRepository.save(salary);

    return this.salariesRepository.findOne({
      where: { id: salary.id },
      relations: ['salaryDetailLists'],
    });
  }

  findAll() {
    return this.salariesRepository.find({ relations: ['salaryDetailLists'] });
  }

  async findOne(id: number) {
    const salary = await this.salariesRepository.findOne({
      where: { id: id },
      relations: ['salaryDetailLists'],
    });
    if (!salary) {
      throw new NotFoundException();
    }
    return salary;
  }

  async update(id: number, updateSalaryDto: UpdateSalaryDto) {
    const salary = await this.salariesRepository.findOneBy({ id: id });
    if (!salary) {
      throw new NotFoundException();
    }
    return this.salariesRepository.save({ ...salary, ...updateSalaryDto });
  }

  async remove(id: number) {
    const salary = await this.salariesRepository.findOne({
      where: { id: id },
      relations: ['salaryDetailLists'],
    });
    if (!salary) {
      throw new NotFoundException();
    }
    if (salary.salaryDetailLists != undefined) {
      // const cios = await this.checkinoutRepository.find({
      //   relations: ['salaryDetail'],
      // });
      // console.log(cios);
      for (const sr of salary.salaryDetailLists) {
        if (salary.salaryDetailLists != undefined) {
          // for (const c of cios) {
          //   if (c.salaryDetail == sr) {
          //     c.salaryDetail = null;
          //     console.log('======');
          //     await this.checkinoutRepository.save(c);
          //   }
          // }
          let cio = await this.checkinoutRepository.findOneBy({
            salaryDetail: { id: sr.id },
          });
          while (cio) {
            if (cio) {
              console.log(cio.salaryDetail);
              cio.salaryDetail = null;
              console.log('======');
              console.log(cio);
              await this.checkinoutRepository.save(cio);
            }
            cio = await this.checkinoutRepository.findOneBy({
              salaryDetail: { id: sr.id },
            });
          }

          // const cio = await this.checkinoutRepository.findOneBy({
          //   salaryDetail: { id: sr.id },
          // });
          // if (cio) {
          //   console.log(cio.salaryDetail);
          //   cio.salaryDetail = null;
          //   console.log('======');
          //   console.log(cio);
          //   await this.checkinoutRepository.save(cio);
          // }
        }
        await this.salaryItemRepository.softRemove(sr);
      }
    }

    return this.salariesRepository.softRemove(salary);
  }
}
