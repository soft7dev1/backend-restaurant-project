import { IsNotEmpty, IsInt, Length, Min } from 'class-validator';

export class CreateTableDto {
  @Min(1)
  @IsInt()
  @IsNotEmpty()
  num: number;

  @Length(3, 12)
  @IsNotEmpty()
  status: string;

  @Min(1)
  @IsInt()
  @IsNotEmpty()
  seat: number;
}
