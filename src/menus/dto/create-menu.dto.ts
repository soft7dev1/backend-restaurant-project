import { IsNotEmpty, MaxLength } from 'class-validator';

export class CreateMenuDto {
  @MaxLength(100)
  @IsNotEmpty()
  name: string;

  @IsNotEmpty()
  price: number;

  @IsNotEmpty()
  status: string;

  @IsNotEmpty()
  categoryName: string;

  image = 'no-image.png';
}
