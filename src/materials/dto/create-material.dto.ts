import { IsNotEmpty, Min, IsInt, IsPositive } from 'class-validator';

export class CreateMaterialDto {
  @IsNotEmpty()
  name: string;

  @Min(1)
  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  min_quantity: number;

  @IsInt()
  @IsNotEmpty()
  quantity: number;

  @IsNotEmpty()
  unit: string;

  @Min(5)
  @IsPositive()
  @IsInt()
  @IsNotEmpty()
  unit_price: number;
}
