import { Module } from '@nestjs/common';
import { CheckMaterialsService } from './check_materials.service';
import { CheckMaterialsController } from './check_materials.controller';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CheckMaterial } from './entities/check_material.entity';
import { CheckMatDetail } from './entities/chmat_detail';
import { Employee } from 'src/employees/entities/employee.entity';
import { Material } from 'src/materials/entities/material.entity';

@Module({
  imports: [
    TypeOrmModule.forFeature([
      CheckMaterial,
      CheckMatDetail,
      Employee,
      Material,
    ]),
  ],
  controllers: [CheckMaterialsController],
  providers: [CheckMaterialsService],
})
export class CheckMaterialsModule {}
