import { Material } from './../../materials/entities/material.entity';
import { BillMaterial } from './bill_material.entity';
import {
  Entity,
  ManyToOne,
  CreateDateColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  Column,
  PrimaryGeneratedColumn,
} from 'typeorm';

@Entity()
export class BillMaterialDetail {
  @PrimaryGeneratedColumn()
  id: number;

  @ManyToOne(() => Material, (material) => material.billItems)
  material: Material; //id

  @Column()
  name: string;
  @Column()
  quantity: number;
  @Column({ type: 'float' })
  price: number;
  @Column({ type: 'float' })
  total: number;

  @ManyToOne(() => BillMaterial, (bill) => bill.billItems)
  bill: BillMaterial;

  @CreateDateColumn()
  createdDate: Date;
  @UpdateDateColumn()
  updatedDate: Date;
  @DeleteDateColumn()
  deletedDate: Date;
}
