import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  UseGuards,
} from '@nestjs/common';
import { JwtAuthGuard } from 'src/auth/jwt-auth.guard';
import { BillMaterialsService } from './bill_materials.service';
import { CreateBillMaterialDto } from './dto/create-bill_material.dto';
import { UpdateBillMaterialDto } from './dto/update-bill_material.dto';

@Controller('bill-materials')
export class BillMaterialsController {
  constructor(private readonly billMaterialsService: BillMaterialsService) {}

  @UseGuards(JwtAuthGuard)
  @Post()
  create(@Body() createBillMaterialDto: CreateBillMaterialDto) {
    return this.billMaterialsService.create(createBillMaterialDto);
  }

  @Get()
  findAll() {
    return this.billMaterialsService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.billMaterialsService.findOne(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Patch(':id')
  update(
    @Param('id') id: string,
    @Body() updateBillMaterialDto: UpdateBillMaterialDto,
  ) {
    return this.billMaterialsService.update(+id, updateBillMaterialDto);
  }

  @UseGuards(JwtAuthGuard)
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.billMaterialsService.remove(+id);
  }

  @UseGuards(JwtAuthGuard)
  @Delete('rmItem/:id')
  removeItem(@Param('id') id: string) {
    return this.billMaterialsService.removeItem(+id);
  }
}
